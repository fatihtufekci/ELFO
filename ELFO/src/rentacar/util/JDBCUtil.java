package rentacar.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JDBCUtil {
	
	private static Properties settings = new Properties();
	private static String fileSeparator;
	private static String filePath = "src/rentacar/util/jdbc.properties";
	
	private static String url;
	private static String ip;
	private static String username;
	private static String password;
	private static String driver;
	
	static{
		fileSeparator = System.getProperty("file.separator");
		FileReader in = null;
		try {
			in = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			System.out.println("Dosya bulunamadı. " + filePath);
		}
		
		try {
			settings.load(in);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		url = settings.getProperty("url");
		username = settings.getProperty("username");
		password = settings.getProperty("password");
		driver = settings.getProperty("driver");
	}
	
	public static synchronized Connection getConnection(){
		Connection connection = null;
		
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			System.out.println(driver + " bulunamadı. " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("Connection alırken problem oluştu. " + e.getMessage());
		}
		
		return connection;
	}
	
	public static void main(String[] args){
		Connection conn = getConnection();
		if (conn != null)
			System.out.println(conn);
		else
			System.out.println("Bağlantı yok!");
	}
	
}
