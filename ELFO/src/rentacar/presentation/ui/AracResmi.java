
package rentacar.presentation.ui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class AracResmi extends JFrame{
    
    JButton araba, geri; 
    ImageIcon image;
    
    public AracResmi(String arac){
        
        super("Araç Resmi");
        
        geri = new JButton("Geri");
        geri.addActionListener(new GeriAction());
        add(geri);
                
        
        image = new ImageIcon("C:\\Users\\Lenovo\\Desktop\\arabalar\\"+arac.toLowerCase()+".jpg");
        
        araba = new JButton(image);
        araba.setSize(1200, 900);
        
        setLayout(new FlowLayout());
        add(araba);
        
        setVisible(true);
        setSize(1500, 1000);
        setLocation(300, 50);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    class GeriAction implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            AraclariListele a = new AraclariListele();
            a.setVisible(true);
        }
    }
    
}
