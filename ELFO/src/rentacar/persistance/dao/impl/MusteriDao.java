
package rentacar.persistance.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import rentacar.business.domain.Musteri;
import rentacar.business.domain.MusteriListele;
import rentacar.persistance.dao.contact.MusteriDaoI;

/**
 *
 * @author Lenovo
 */
public class MusteriDao extends AbstractDao implements MusteriDaoI{
    
    private ResultSet rs;

    @Override
    public MusteriListele musteriListele() {
       getConnection();
		MusteriListele listele = new MusteriListele();
		try {
			ArrayList<Musteri> list = new ArrayList<>();
			pstmt = conn.prepareStatement("select * from musteri");
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				Musteri m = new Musteri();
				m.setTc(rs.getLong(1));
				m.setAd(rs.getString(2));
				m.setSoyad(rs.getString(3));
				m.setTelefon(rs.getLong(4));
				m.setAdres(rs.getString(5));
				list.add(m);
			}
			listele.setList(list);
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
			}
		}
		
		return listele;
    }

        @Override
    public boolean musteriEkle(Musteri m) {
                getConnection();
		boolean b=false;
		
		try {
			pstmt = conn.prepareStatement("insert into musteri(TC, Ad, Soyad, Telefon, Adres) values(?,?,?,?,?)");
			pstmt.setLong(1, m.getTc());
			pstmt.setString(2, m.getAd());
			pstmt.setString(3, m.getSoyad());
			pstmt.setLong(4, m.getTelefon());
			pstmt.setString(5, m.getAdres());
			
			pstmt.execute();
			
                        //JOptionPane.showMessageDialog(null, "Müşteri kaydedildi.");
                        
			pstmt.close();
                        conn.close();
                        b = true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return b;
	}

    @Override
    public void musteriSil(long tc) {
        
        try {
            getConnection();
            
            String query = "delete from musteri where TC=?";
            
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, String.valueOf(tc));
            pstmt.execute();
            
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
			System.out.println(ex.getMessage());
        }finally {
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
        
    }
    
    
}
