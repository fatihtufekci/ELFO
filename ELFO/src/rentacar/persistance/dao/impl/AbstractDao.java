/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rentacar.persistance.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import rentacar.util.JDBCUtil;

/**
 *
 * @author Lenovo
 */
public abstract class AbstractDao {
        protected Connection conn;
	protected PreparedStatement pstmt;

	protected void getConnection(){
		conn = JDBCUtil.getConnection();
	}
}
