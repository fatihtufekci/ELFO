
package rentacar.persistance.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import rentacar.business.domain.Personel;
import rentacar.persistance.dao.contact.PersonelDaoI;

/**
 *
 * @author Lenovo
 */
public class PersonelDao extends AbstractDao implements PersonelDaoI{
    
    ResultSet rs;
    
    @Override
    public boolean personelGirisi(Personel p) {
        getConnection();
        String sql = "select * from personel";
        String username = p.getUsername();
        String password = p.getPassword();
        
        boolean checkUsername = false;
        boolean checkPassword = false;
        
        try {
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {                
                if(username.equals(rs.getString("Username"))){
                    checkUsername = true;
                }
                if(password.equals(rs.getString("Password"))){
                    checkPassword = true;
                }
            }
            
            if( !(checkUsername) || !(checkPassword)){
                    JOptionPane.showMessageDialog(null, "Username veya password yanlış");
                    return false;
            }
            
            rs.close();
            pstmt.close();
            conn.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally{
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(" Bağlantı kapatılamadı! " + ex.getMessage());
            }
        }
        
        return true;
    }

    @Override
    public void parolaBelirle(Personel p) {
        getConnection();
        String query = "update personel set Username=?, Password=? where TC=?";
        try {
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, p.getUsername());
            pstmt.setString(2, p.getPassword());
            pstmt.setString(3, String.valueOf(p.getTc()));
            
            pstmt.execute();
            
            pstmt.close();
            conn.close();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }finally{
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    
}
