
package rentacar.persistance.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import rentacar.business.domain.Araba;
import rentacar.business.domain.ArabaListele;
import rentacar.business.domain.Gecmis;
import rentacar.business.domain.GecmisListele;
import rentacar.persistance.dao.contact.ArabaDaoI;

/**
 *
 * @author Lenovo
 */
public class ArabaDao extends AbstractDao implements ArabaDaoI{
    	private ResultSet rs, rs2;
	private PreparedStatement pstmt2, pstmt3;
        
	@Override
	public ArabaListele arabaListele() {
		getConnection();
                String sql = "select * from araba2 where durum=0";
                
                ArabaListele a = listele(sql);
                
                return a;
	}


    @Override
    public void aracEkle(Araba araba) {
                getConnection();
		try {
			pstmt = conn.prepareStatement("insert into araba2(Plaka, Marka, Model, Beygir, MotorHacmi, KM, Yil, GunlukUcreti, Durum) "
					+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, araba.getPlaka());
			pstmt.setString(2, araba.getMarka());
			pstmt.setString(3, araba.getModel());
			pstmt.setInt(4, araba.getBeygir());
			pstmt.setInt(5, araba.getMotorHacmi());
			pstmt.setInt(6, araba.getKm());
			pstmt.setInt(7, araba.getYil());
			pstmt.setInt(8, araba.getGunlukUcreti());
			pstmt.setString(9, String.valueOf(0));
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Arac kaydedildi.");
			
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
			}
		}
    }

    @Override
    public void arabaKirala(Araba a) {
        getConnection();
        String query1 = "update araba2 set AlisTarihi=?, TeslimTarihi=?, MusteriTC=?, Durum=1 where id=?";
        String query2 = "select GunlukUcreti from araba2 where id=?";
        
        try {
            pstmt = conn.prepareStatement(query1);
            
            pstmt.setString(1, a.getAlisTarihi());
            pstmt.setString(2, a.getTeslimTarihi());
            pstmt.setString(3, String.valueOf(a.getMusteriTC()));
            pstmt.setString(4, String.valueOf(a.getId()));
            
            pstmt.execute();
            
            pstmt = conn.prepareStatement(query2);
            pstmt.setString(1, String.valueOf(a.getId()));
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                a.setGunlukUcreti(rs.getInt(1));
            }
            
            JOptionPane.showMessageDialog(null, "Müşteri kaydedildi ve Araç müşteriye kiralandı.");
            
            
            pstmt.close();
            conn.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally{
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
		}
	}
    }

    @Override
    public ArabaListele tumAraclariListele() {
        getConnection();
        String sql = "select * from araba2";
        ArabaListele a = listele(sql);
        return a;
    }
    
    @Override
    public void guncelle(int id){
        
        getConnection();
        
        String query = "update araba2 set Durum=0 where id=?";
        
        
        try {
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, String.valueOf(id));
            pstmt.execute();
            
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());    
        }finally{
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
		}
	}
        
    }
    
    @Override
    public void gecmis(long tc){
        getConnection();
        
        String query1 = "select * from araba2 where MusteriTC="+tc;
        String query2 = "select Ad, Soyad from musteri where TC="+tc;
        String query3 = "insert into islemler(TC, Ad, Soyad, Plaka, Marka, Model, AlisTarihi, TeslimTarihi, Ucret) values(?,?,?,?,?,?,?,?,?)";
        String ad="";
        String soyad="";
        
                try {
                    pstmt = conn.prepareStatement(query1);
                    rs = pstmt.executeQuery();
                    
                    while(rs.next()){
                        pstmt2 = conn.prepareStatement(query2);
                        rs2 = pstmt2.executeQuery();
                        while(rs2.next()){
                            ad = rs2.getString("Ad");
                            soyad = rs2.getString("Soyad");
                        }
                        
                        pstmt3 = conn.prepareStatement(query3);
                        pstmt3.setString(1, String.valueOf(tc));
                        pstmt3.setString(2, ad);
                        pstmt3.setString(3, soyad);
                        pstmt3.setString(4, rs.getString("Plaka"));
                        pstmt3.setString(5, rs.getString("Marka"));
                        pstmt3.setString(6, rs.getString("Model"));
                        pstmt3.setString(7, rs.getString("AlisTarihi"));
                        pstmt3.setString(8, rs.getString("TeslimTarihi"));
                        pstmt3.setString(9, String.valueOf(rs.getInt("GunlukUcreti")));
                        pstmt3.execute();
                    }
                    rs.close();
                    rs2.close();
                    pstmt.close();
                    pstmt3.close();
                    pstmt2.close();
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }finally{
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
			}
		}
    }
    
    public ArabaListele listele(String sql){
                ArabaListele listele = new ArabaListele();
		try {
			ArrayList<Araba> list = new ArrayList<>();
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				Araba a = new Araba();
				a.setId(rs.getInt(1));
				a.setPlaka(rs.getString(2));
				a.setMarka(rs.getString(3));
				a.setModel(rs.getString(4));
				a.setBeygir(rs.getInt(5));
				a.setMotorHacmi(rs.getInt(6));
				a.setKm(rs.getInt(7));
				a.setYil(rs.getInt(8));
                                a.setAlisTarihi(rs.getString(9));
                                a.setTeslimTarihi(rs.getString(10));
				a.setGunlukUcreti(rs.getInt(11));
				a.setMusteriTC(rs.getLong(13));
                                a.setDurum(rs.getBoolean(12));
                                
//				m.setTc(rs.getLong(1));
				list.add(a);
			}
			listele.setList(list);
			
			rs.close();
			pstmt.close();
                        conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
			}
		}
		
		return listele;
    }

    @Override
    public GecmisListele gecmisListele() {
        getConnection();
        GecmisListele listele = new GecmisListele();
        ArrayList<Gecmis> list = new ArrayList<>();
    
        String query = "select * from islemler";
        
        try {
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                Gecmis g = new Gecmis();
                g.setTc(Long.parseLong(rs.getString("TC")));
                g.setAd(rs.getString("Ad"));
                g.setSoyad(rs.getString("Soyad"));
                g.setPlaka(rs.getString("Plaka"));
                g.setMarka(rs.getString("Marka"));
                g.setModel(rs.getString("Model"));
                g.setAlisTarihi(rs.getString("AlisTarihi"));
                g.setTeslimTarihi(rs.getString("TeslimTarihi"));
                g.setUcret(rs.getInt("Ucret"));
                
                list.add(g);
            }
            
            listele.setList(list);
            
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }finally{
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println("Bağlantı kapatılamadı! " + e.getMessage());
			}
		}
        
        return listele;
    }

}
