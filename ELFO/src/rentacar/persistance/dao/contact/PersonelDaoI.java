
package rentacar.persistance.dao.contact;

import rentacar.business.domain.Personel;

/**
 *
 * @author Lenovo
 */
public interface PersonelDaoI {
    public boolean personelGirisi(Personel p);
    public void parolaBelirle(Personel p);
}
