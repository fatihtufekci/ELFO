
package rentacar.persistance.dao.contact;

import rentacar.business.domain.Musteri;
import rentacar.business.domain.MusteriListele;

/**
 *
 * @author Lenovo
 */
public interface MusteriDaoI {
	public MusteriListele musteriListele();
	public boolean musteriEkle(Musteri m);
        public void musteriSil(long tc);
}
