
package rentacar.persistance.dao.contact;

import rentacar.business.domain.Araba;
import rentacar.business.domain.ArabaListele;
import rentacar.business.domain.GecmisListele;

/**
 *
 * @author Lenovo
 */
public interface ArabaDaoI {
    	public ArabaListele arabaListele();
        public ArabaListele tumAraclariListele();
	public void aracEkle(Araba araba);
        public void arabaKirala(Araba a);
        public void guncelle(int id);
        public void gecmis(long tc);
        public GecmisListele gecmisListele();
}
