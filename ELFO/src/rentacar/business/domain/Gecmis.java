
package rentacar.business.domain;

public class Gecmis {
    
    private long tc;
    private String ad, soyad, plaka, marka, model, alisTarihi, teslimTarihi;
    private int ucret;
    
    public int getUcret(){
        return ucret;
    }
    
    public void setUcret(int ucret){
        this.ucret = ucret;
    }
    
    public long getTc() {
        return tc;
    }

    public void setTc(long tc) {
        this.tc = tc;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getPlaka() {
        return plaka;
    }

    public void setPlaka(String plaka) {
        this.plaka = plaka;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAlisTarihi() {
        return alisTarihi;
    }

    public void setAlisTarihi(String alisTarihi) {
        this.alisTarihi = alisTarihi;
    }

    public String getTeslimTarihi() {
        return teslimTarihi;
    }

    public void setTeslimTarihi(String teslimTarihi) {
        this.teslimTarihi = teslimTarihi;
    }


}
