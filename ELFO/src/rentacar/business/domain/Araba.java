package rentacar.business.domain;

public class Araba {
	
	private int id;
	private String plaka;
	private String marka;
	private String model;
	private int beygir;
	private int motorHacmi;
	private int km;
	private int yil;
	private String alisTarihi = "";
	private String teslimTarihi = "";
	private int gunlukUcreti;
	private boolean durum;
	private Long musteriTC;
	
        public Araba(){
            id=-1;
            plaka = "";
            marka = "";
            model = "";
            beygir = 0;
            motorHacmi = 0;
            km = 0;
            yil = 0;
            gunlukUcreti = 0;
            durum = false;
            alisTarihi = "";
            teslimTarihi = "";
        }
        
	public Long getMusteriTC() {
		return musteriTC;
	}
	public void setMusteriTC(Long musteriTC) {
		this.musteriTC = musteriTC;
	}
	
	
	public String getAlisTarihi() {
		return alisTarihi;
	}
	public void setAlisTarihi(String alisTarihi) {
		this.alisTarihi = alisTarihi;
	}
	public String getTeslimTarihi() {
		return teslimTarihi;
	}
	public void setTeslimTarihi(String teslimTarihi) {
		this.teslimTarihi = teslimTarihi;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPlaka() {
		return plaka;
	}
	public void setPlaka(String plaka) {
		this.plaka = plaka;
	}
	public String getMarka() {
		return marka;
	}
	public void setMarka(String marka) {
		this.marka = marka;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getBeygir() {
		return beygir;
	}
	public void setBeygir(int beygir) {
		this.beygir = beygir;
	}
	public int getMotorHacmi() {
		return motorHacmi;
	}
	public void setMotorHacmi(int motorHacmi) {
		this.motorHacmi = motorHacmi;
	}
	public int getKm() {
		return km;
	}
	public void setKm(int km) {
		this.km = km;
	}
	public int getYil() {
		return yil;
	}
	public void setYil(int yil) {
		this.yil = yil;
	}

	public int getGunlukUcreti() {
		return gunlukUcreti;
	}
	public void setGunlukUcreti(int gunlukUcreti) {
		this.gunlukUcreti = gunlukUcreti;
	}
	public boolean isDurum() {
		return durum;
	}
	public void setDurum(boolean durum) {
		this.durum = durum;
	}
	
}
