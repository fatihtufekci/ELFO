package rentacar.business.domain;

public class Personel {
	
	private long tc;
	private String ad;
	private String soyad;
        private String username;
	private String password;
	private long telefon;
	
        
        public String getUsername(){
            return username;
        }
        public void setUsername(String username){
            this.username = username;
        }
        
	public long getTc() {
		return tc;
	}
	public void setTc(long tc) {
		this.tc = tc;
	}
	public String getAd() {
		return ad;
	}
	public void setAd(String ad) {
		this.ad = ad;
	}
	public String getSoyad() {
		return soyad;
	}
	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getTelefon() {
		return telefon;
	}
	public void setTelefon(long telefon) {
		this.telefon = telefon;
	}
	
}
