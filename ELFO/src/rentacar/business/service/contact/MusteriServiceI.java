
package rentacar.business.service.contact;

import rentacar.business.domain.Musteri;
import rentacar.business.domain.MusteriListele;

/**
 *
 * @author Lenovo
 */
public interface MusteriServiceI {
    	public MusteriListele musteriListele();
	public boolean musteriEkle(Musteri musteri);
        public void musteriSil(long tc);
}
