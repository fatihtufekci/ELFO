
package rentacar.business.service.contact;

import rentacar.business.domain.Araba;
import rentacar.business.domain.ArabaListele;
import rentacar.business.domain.GecmisListele;

/**
 *
 * @author Lenovo
 */
public interface ArabaServiceI {
    	public void aracEkle(Araba araba);
	public ArabaListele arabaListele();
        public void arabaKirala(Araba a);
        public ArabaListele tumAraclariListele();
        public void guncelle(int id);
        public void gecmis(long tc);
        public GecmisListele gecmisListele();
}
