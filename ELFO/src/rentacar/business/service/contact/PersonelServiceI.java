
package rentacar.business.service.contact;

import rentacar.business.domain.Personel;

/**
 *
 * @author Lenovo
 */
public interface PersonelServiceI {
    public boolean personelGirisi(Personel p);
    public void parolaBelirle(Personel p);
}
