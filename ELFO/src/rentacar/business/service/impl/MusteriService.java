
package rentacar.business.service.impl;

import rentacar.business.domain.Musteri;
import rentacar.business.domain.MusteriListele;
import rentacar.business.service.contact.MusteriServiceI;
import rentacar.persistance.dao.contact.MusteriDaoI;
import rentacar.persistance.dao.impl.MusteriDao;

/**
 *
 * @author Lenovo
 */
public class MusteriService implements MusteriServiceI{
    
    private MusteriDaoI musteriDao = new MusteriDao();

    @Override
    public MusteriListele musteriListele() {
        return musteriDao.musteriListele();
    }

    @Override
    public boolean musteriEkle(Musteri m) {
        return musteriDao.musteriEkle(m);
    }

    @Override
    public void musteriSil(long tc) {
        musteriDao.musteriSil(tc);
    }
    
}
