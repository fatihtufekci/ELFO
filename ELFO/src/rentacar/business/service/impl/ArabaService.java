
package rentacar.business.service.impl;

import rentacar.business.domain.Araba;
import rentacar.business.domain.ArabaListele;
import rentacar.business.domain.GecmisListele;
import rentacar.business.service.contact.ArabaServiceI;
import rentacar.persistance.dao.contact.ArabaDaoI;
import rentacar.persistance.dao.impl.ArabaDao;

/**
 *
 * @author Lenovo
 */
public class ArabaService implements ArabaServiceI{
    
    private ArabaDaoI arabaDao = new ArabaDao();
    
    @Override
    public void aracEkle(Araba araba) {
        arabaDao.aracEkle(araba);
    }


    @Override
    public ArabaListele arabaListele() {
       return arabaDao.arabaListele();
    }

    @Override
    public void arabaKirala(Araba a) {
        arabaDao.arabaKirala(a);
    }

    @Override
    public ArabaListele tumAraclariListele() {
        return arabaDao.tumAraclariListele();
    }

    @Override
    public void guncelle(int id) {
        arabaDao.guncelle(id);
    }

    @Override
    public void gecmis(long tc) {
        arabaDao.gecmis(tc);
    }

    @Override
    public GecmisListele gecmisListele() {
        return arabaDao.gecmisListele();
    }
    
}
