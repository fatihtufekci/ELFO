/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rentacar.business.service.impl;

import rentacar.business.domain.Personel;
import rentacar.business.service.contact.PersonelServiceI;
import rentacar.persistance.dao.contact.PersonelDaoI;
import rentacar.persistance.dao.impl.PersonelDao;

/**
 *
 * @author Lenovo
 */
public class PersonelService implements PersonelServiceI{

    PersonelDaoI personelDao = new PersonelDao();
    
    @Override
    public boolean personelGirisi(Personel p) {
        return personelDao.personelGirisi(p);
    }

    @Override
    public void parolaBelirle(Personel p) {
        personelDao.parolaBelirle(p);
    }
    
}
