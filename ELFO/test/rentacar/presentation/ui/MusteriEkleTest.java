/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rentacar.presentation.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class MusteriEkleTest {
    
    public MusteriEkleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of controlled method, of class MusteriEkle.
     */
    @Test
    public void testControlled() {
        System.out.println("controlled");
        MusteriEkle instance = new MusteriEkle();
        // alıs yıl, ay, gun ve teslim yıl, ay, gun
        int expResult = 6;
        int result = instance.controlled();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSqlTarihFormat method, of class MusteriEkle.
     */
    @Test
    public void testGetSqlTarihFormat() {
        System.out.println("getSqlTarihFormat");
        String y = "2018";
        String a = "10";
        String g = "5";
        MusteriEkle instance = new MusteriEkle();
        String expResult = "2018-10-5";
        String result = instance.getSqlTarihFormat(y, a, g);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDateFormat method, of class MusteriEkle.
     */
    @Test
    public void testGetDateFormat() {
        System.out.println("getDateFormat");
        String y = "2018";
        String a = "10";
        String g = "5";
        MusteriEkle instance = new MusteriEkle();
        String expResult = "2018/10/5";
        String result = instance.getDateFormat(y, a, g);
        assertEquals(expResult, result);
    }
    /**
     * Test of formatla method, of class MusteriEkle.
     */
    @Test
    public void testFormatla() {
        System.out.println("formatla");
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        String s = "2018/12/15";
        MusteriEkle instance = new MusteriEkle();
        Date expResult=null;
        try {
            expResult = format.parse(s);
        } catch (ParseException ex) {
            System.err.print(ex.getMessage());
        }
        Date result = instance.formatla(s);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of musteriEkle method, of class MusteriEkle.
     */
    @Test
    public void testMusteriEkle() {
        System.out.println("musteriEkle");
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = true;
        boolean result = instance.musteriEkle();
        assertEquals(expResult, result);
    }

        /**
     * Test of textTemizle method, of class MusteriEkle.
     */
    @Test
    public void testTextTemizle() {
        System.out.println("textTemizle");
        MusteriEkle instance = new MusteriEkle();
        instance.textTemizle();
    }
    
    /**
     * Test of getDifferenceDays method, of class MusteriEkle.
     */
    @Test
    public void testGetDifferenceDays() {
        System.out.println("getDifferenceDays");
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse("2018/01/01");
            d2 = format.parse("2018/01/03");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        MusteriEkle instance = new MusteriEkle();
        int expResult = 2;
        int result = instance.getDifferenceDays(d1, d2);
        assertEquals(expResult, result);
    }
    
    @Test(expected=NullPointerException.class)
    public void testGetDifferenceDays2() {
        System.out.println("getDifferenceDays2");
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse("2018-01-1");
            d2 = format.parse("2018/01/3");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        MusteriEkle instance = new MusteriEkle();
        int expResult = 2;
        int result = instance.getDifferenceDays(d1, d2);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkTCKN method, of class MusteriEkle.
     */
    @Test
    public void testCheckTCKN() {
        System.out.println("checkTCKN");
        String txtTCKN = "96385912345";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = true;
        boolean result = instance.checkTCKN(txtTCKN);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkTelefon method, of class MusteriEkle.
     */
    @Test
    public void testCheckTelefon() {
        System.out.println("checkTelefon");
        String txtTelefon = "123";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkTelefon(txtTelefon);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkAd method, of class MusteriEkle.
     */
    @Test
    public void testCheckAd() {
        System.out.println("checkAd");
        String txtAd = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkAd(txtAd);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkSoyad method, of class MusteriEkle.
     */
    @Test
    public void testCheckSoyad() {
        System.out.println("checkSoyad");
        String txtSoyad = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkSoyad(txtSoyad);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkAdres method, of class MusteriEkle.
     */
    @Test
    public void testCheckAdres() {
        System.out.println("checkAdres");
        String txtAdres = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkAdres(txtAdres);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkYil method, of class MusteriEkle.
     */
    @Test
    public void testCheckYil() {
        System.out.println("checkYil");
        String txt = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkYil(txt);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkAy method, of class MusteriEkle.
     */
    @Test
    public void testCheckAy() {
        System.out.println("checkAy");
        String txt = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkAy(txt);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkGun method, of class MusteriEkle.
     */
    @Test
    public void testCheckGun() {
        System.out.println("checkGun");
        String txt = "";
        MusteriEkle instance = new MusteriEkle();
        boolean expResult = false;
        boolean result = instance.checkGun(txt);
        assertEquals(expResult, result);
    }
    
}
