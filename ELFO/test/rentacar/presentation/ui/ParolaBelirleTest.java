
package rentacar.presentation.ui;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class ParolaBelirleTest {
    
    public ParolaBelirleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of checkTC method, of class ParolaBelirle.
     */
    @Test
    public void testCheckTC() {
        System.out.println("checkTC");
        String t = "";
        ParolaBelirle instance = new ParolaBelirle();
        boolean expResult = false;
        boolean result = instance.checkTC(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkUsername method, of class ParolaBelirle.
     */
    @Test
    public void testCheckUsername() {
        System.out.println("checkUsername");
        String t = "";
        ParolaBelirle instance = new ParolaBelirle();
        boolean expResult = false;
        boolean result = instance.checkUsername(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkPassword method, of class ParolaBelirle.
     */
    @Test
    public void testCheckPassword() {
        System.out.println("checkPassword");
        String t = "";
        ParolaBelirle instance = new ParolaBelirle();
        boolean expResult = false;
        boolean result = instance.checkPassword(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashPassword method, of class ParolaBelirle.
     */
    @Test
    public void testHashPassword() throws Exception {
        System.out.println("hashPassword");
        String password = "cengiz";
        ParolaBelirle instance = new ParolaBelirle();
        String expResult = "4fa62557bf59ccc94e34a4ee357b91fccdafc";
        String result = instance.hashPassword(password);
        assertEquals(expResult, result);
    }
    
}
