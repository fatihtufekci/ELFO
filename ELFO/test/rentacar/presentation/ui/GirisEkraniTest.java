/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rentacar.presentation.ui;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class GirisEkraniTest {
    
    public GirisEkraniTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
        /**
     * Test of controlled method, of class GirisEkrani.
     */
    @Test
    public void testControlled() {
        System.out.println("controlled");
        GirisEkrani instance = new GirisEkrani();
        // alıs yıl, ay, gun ve teslim yıl, ay, gun
        int expResult = 0;
        int result = instance.controlled();
        assertEquals(expResult, result);
    }

    /**
     * Test of checkUsername method, of class GirisEkrani.
     */
    @Test
    public void testCheckUsername() {
        System.out.println("checkUsername");
        String t = "";
        GirisEkrani instance = new GirisEkrani();
        boolean expResult = false;
        boolean result = instance.checkUsername(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkPassword method, of class GirisEkrani.
     */
    @Test
    public void testCheckPassword() {
        System.out.println("checkPassword");
        String t = "";
        GirisEkrani instance = new GirisEkrani();
        boolean expResult = false;
        boolean result = instance.checkPassword(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashPassword method, of class GirisEkrani.
     */
    @Test
    public void testHashPassword() throws Exception {
        System.out.println("hashPassword");
        String password = "cengiz";
        GirisEkrani instance = new GirisEkrani();
        String expResult = "4fa62557bf59ccc94e34a4ee357b91fccdafc";
        String result = instance.hashPassword(password);
        assertEquals(expResult, result);
    }

    
}
