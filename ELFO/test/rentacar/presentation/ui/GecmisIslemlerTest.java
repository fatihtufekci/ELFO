/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rentacar.presentation.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class GecmisIslemlerTest {
    
    public GecmisIslemlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getDifferenceDays method, of class GecmisIslemler.
     */
    @Test
    public void testGetDifferenceDays() {
        System.out.println("getDifferenceDays");
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse("2018/01/01");
            d2 = format.parse("2018/01/03");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        MusteriEkle instance = new MusteriEkle();
        int expResult = 2;
        int result = instance.getDifferenceDays(d1, d2);
        assertEquals(expResult, result);
    }
    
}
