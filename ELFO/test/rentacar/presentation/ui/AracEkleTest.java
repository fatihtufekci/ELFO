
package rentacar.presentation.ui;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class AracEkleTest {
    
    public AracEkleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Test of controlled method, of class AracEkle.
     */
    @Test
    public void testControlled() {
        System.out.println("controlled");
        AracEkle instance = new AracEkle();
        int expResult = 0;
        int result = instance.controlled();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of arabaEkle method, of class AracEkle.
     */
    @Test
    public void testArabaEkle() {
        System.out.println("arabaEkle");
        AracEkle instance = new AracEkle();
        instance.arabaEkle();
    }
    
    /**
     * Test of textTemizle method, of class AracEkle.
     */
    @Test
    public void testTextTemizle() {
        System.out.println("textTemizle");
        AracEkle instance = new AracEkle();
        instance.textTemizle();
    }
    
    /**
     * Test of checkGunlukUcreti method, of class AracEkle.
     */
    @Test
    public void testCheckGunlukUcreti() {
        System.out.println("checkGunlukUcreti");
        String txtGunlukUcret = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkGunlukUcreti(txtGunlukUcret);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkYil method, of class AracEkle.
     */
    @Test
    public void testCheckYil() {
        System.out.println("checkYil");
        String txtYil = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkYil(txtYil);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkKm method, of class AracEkle.
     */
    @Test
    public void testCheckKm() {
        System.out.println("checkKm");
        String txtKm = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkKm(txtKm);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkMotorHacmi method, of class AracEkle.
     */
    @Test
    public void testCheckMotorHacmi() {
        System.out.println("checkMotorHacmi");
        String txtMotor = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkMotorHacmi(txtMotor);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkBeygir method, of class AracEkle.
     */
    @Test
    public void testCheckBeygir() {
        System.out.println("checkBeygir");
        String txtBeygir = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkBeygir(txtBeygir);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkModel method, of class AracEkle.
     */
    @Test
    public void testCheckModel() {
        System.out.println("checkModel");
        String txtModel = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkModel(txtModel);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkMarka method, of class AracEkle.
     */
    @Test
    public void testCheckMarka() {
        System.out.println("checkMarka");
        String txtMarka = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkMarka(txtMarka);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkPlaka method, of class AracEkle.
     */
    @Test
    public void testCheckPlaka() {
        System.out.println("checkPlaka");
        String txtPlaka = "";
        AracEkle instance = new AracEkle();
        boolean expResult = false;
        boolean result = instance.checkPlaka(txtPlaka);
        assertEquals(expResult, result);
    }
    
}
