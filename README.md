>  Araç Kiralama Sistemi

**Proje Tanımı:** Bu program veri tabanına bağlanarak kullanıcı girişi, araç ekleme, araç görüntüleme, araç silme, verilen tarih aralığında aracı rezerve etme ve bu bilgilere göre fiyat bilgisi getirme gibi işlemler yapmaktadır. Veri tabanı olarak MySql kullanılmıştır. Kullanıcı girişi için kullanılan parolalar SHA1 algoritmasıyla şifrelendikten sonra veritabanına kaydedilmektedir. Kullanıcı arayüzü oluşturulurken de Java Swing kullanılmıştır. 

**Proje Ekran Alıntıları:** (https://gitlab.com/fatihtufekci/ELFO/tree/master/img)