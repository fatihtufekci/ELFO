-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rentacar
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `araba`
--

DROP TABLE IF EXISTS `araba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `araba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Plaka` char(8) NOT NULL,
  `Marka` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `Beygir` int(11) NOT NULL,
  `MotorHacmi` int(11) NOT NULL,
  `KM` int(11) NOT NULL,
  `Yil` int(11) NOT NULL,
  `AlisTarihi` date DEFAULT NULL,
  `TeslimTarihi` date DEFAULT NULL,
  `GunlukUcreti` int(11) NOT NULL,
  `Durum` tinyint(1) NOT NULL,
  `MusteriTC` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MusteriTC` (`MusteriTC`),
  CONSTRAINT `araba_ibfk_1` FOREIGN KEY (`MusteriTC`) REFERENCES `musteri` (`TC`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `araba`
--

LOCK TABLES `araba` WRITE;
/*!40000 ALTER TABLE `araba` DISABLE KEYS */;
INSERT INTO `araba` VALUES (6,'34yz8111','Renault','Symbol',100,1400,29000,2015,NULL,NULL,100,0,NULL),(7,'34tb7869','Ford','Taounus',180,1900,200000,1985,NULL,NULL,90,0,NULL);
/*!40000 ALTER TABLE `araba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `araba2`
--

DROP TABLE IF EXISTS `araba2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `araba2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Plaka` char(8) NOT NULL,
  `Marka` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `Beygir` int(11) NOT NULL,
  `MotorHacmi` int(11) NOT NULL,
  `KM` int(11) NOT NULL,
  `Yil` int(11) NOT NULL,
  `AlisTarihi` date DEFAULT NULL,
  `TeslimTarihi` date DEFAULT NULL,
  `GunlukUcreti` int(11) NOT NULL,
  `Durum` tinyint(1) NOT NULL,
  `MusteriTC` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `araba2`
--

LOCK TABLES `araba2` WRITE;
/*!40000 ALTER TABLE `araba2` DISABLE KEYS */;
INSERT INTO `araba2` VALUES (1,'34as5455','Mercedes','CLA200',300,2500,25000,2014,'2018-05-07','2018-05-07',140,0,0),(2,'34bn9878','BMW','i5',250,3000,40000,2016,'2018-05-07','2018-05-07',150,0,0),(3,'34hn9087','Tofaş','Sahin',100,1500,200000,1997,'2018-05-08','2018-05-09',90,1,56756756756),(4,'34gh4567','Volkswagen','Golf',200,2000,20000,2016,'2018-05-07','2018-05-07',140,0,0),(5,'34th7867','Seat','Leon',200,2000,30000,2018,'2018-05-08','2018-05-11',150,1,76567897654),(8,'34fth04','Audi','Q3',250,2500,30000,2015,'2018-05-07','2018-05-07',160,0,0),(9,'34nt5467','BMW','i5',300,3000,25000,2017,'2018-06-08','2018-05-09',200,1,12345678985),(10,'34PT4565','BMW','M3',180,2000,20000,2015,'2018-05-08','2018-05-12',300,1,23423423423),(11,'34as5455','Mercedes','CLA200',300,2500,25000,2014,NULL,NULL,140,0,NULL);
/*!40000 ALTER TABLE `araba2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `islemler`
--

DROP TABLE IF EXISTS `islemler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `islemler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TC` bigint(11) NOT NULL,
  `Ad` varchar(45) NOT NULL,
  `Soyad` varchar(45) NOT NULL,
  `Plaka` char(8) NOT NULL,
  `Marka` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `AlisTarihi` date NOT NULL,
  `TeslimTarihi` date NOT NULL,
  `Ucret` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `islemler`
--

LOCK TABLES `islemler` WRITE;
/*!40000 ALTER TABLE `islemler` DISABLE KEYS */;
INSERT INTO `islemler` VALUES (1,65478992344,'Halil','Özel','34fth04','Audi','Q3','2018-05-07','2018-05-08',160),(2,45678909876,'Murat','Orak','34nt5467','BMW','i5','2018-05-07','2018-05-08',200),(3,76567897654,'Onur','Mavi','34th7867','Seat','Leon','2018-05-08','2018-05-11',150),(4,56756756756,'Yavuz','Sümbül','34hn9087','Tofaş','Sahin','2018-05-08','2018-05-09',90),(5,23423423423,'Selo','Sarı','34PT4565','BMW','M3','2018-05-08','2018-05-12',300),(6,12345678985,'dasdsa','dsadsa','34nt5467','BMW','i5','2018-06-08','2018-05-09',200);
/*!40000 ALTER TABLE `islemler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musteri`
--

DROP TABLE IF EXISTS `musteri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musteri` (
  `TC` bigint(11) NOT NULL,
  `Ad` varchar(25) NOT NULL,
  `Soyad` varchar(50) NOT NULL,
  `Telefon` char(11) NOT NULL,
  `Adres` varchar(100) NOT NULL,
  PRIMARY KEY (`TC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musteri`
--

LOCK TABLES `musteri` WRITE;
/*!40000 ALTER TABLE `musteri` DISABLE KEYS */;
INSERT INTO `musteri` VALUES (12345678985,'dasdsa','dsadsa','12343434343','İstanbul'),(23423423423,'Selo','Sarı','45645667856','Rize'),(56756756756,'Yavuz','Sümbül','67845623456','Tokat'),(76567897654,'Onur','Mavi','76534555443','Kayseri');
/*!40000 ALTER TABLE `musteri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `musteriaraba`
--

DROP TABLE IF EXISTS `musteriaraba`;
/*!50001 DROP VIEW IF EXISTS `musteriaraba`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `musteriaraba` AS SELECT 
 1 AS `TC`,
 1 AS `Ad`,
 1 AS `Soyad`,
 1 AS `Telefon`,
 1 AS `Plaka`,
 1 AS `Marka`,
 1 AS `Model`,
 1 AS `AlisTarihi`,
 1 AS `TeslimTarihi`,
 1 AS `GunlukUcreti`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `personel`
--

DROP TABLE IF EXISTS `personel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personel` (
  `TC` bigint(11) NOT NULL,
  `Ad` varchar(25) NOT NULL,
  `Soyad` varchar(45) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Telefon` char(11) NOT NULL,
  PRIMARY KEY (`TC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personel`
--

LOCK TABLES `personel` WRITE;
/*!40000 ALTER TABLE `personel` DISABLE KEYS */;
INSERT INTO `personel` VALUES (12345678987,'Fatih','Tüfekçi','tufekci','40bd0156385fc35165329ea1ff5c5ecbdbbeef','44444444444'),(99999999999,'Cengiz','Under','cengiz','6216f8a75fd5bb3d5f22b6f9958cdede3fc086c2','55555555555');
/*!40000 ALTER TABLE `personel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rentacar'
--

--
-- Dumping routines for database 'rentacar'
--

--
-- Final view structure for view `musteriaraba`
--

/*!50001 DROP VIEW IF EXISTS `musteriaraba`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `musteriaraba` AS (select `m`.`TC` AS `TC`,`m`.`Ad` AS `Ad`,`m`.`Soyad` AS `Soyad`,`m`.`Telefon` AS `Telefon`,`a`.`Plaka` AS `Plaka`,`a`.`Marka` AS `Marka`,`a`.`Model` AS `Model`,`a`.`AlisTarihi` AS `AlisTarihi`,`a`.`TeslimTarihi` AS `TeslimTarihi`,`a`.`GunlukUcreti` AS `GunlukUcreti` from (`musteri` `m` join `araba` `a` on((`m`.`TC` = `a`.`MusteriTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  2:03:24
