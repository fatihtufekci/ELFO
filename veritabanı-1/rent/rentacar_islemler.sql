-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rentacar
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `islemler`
--

DROP TABLE IF EXISTS `islemler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `islemler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TC` bigint(11) NOT NULL,
  `Ad` varchar(45) NOT NULL,
  `Soyad` varchar(45) NOT NULL,
  `Plaka` char(8) NOT NULL,
  `Marka` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `AlisTarihi` date NOT NULL,
  `TeslimTarihi` date NOT NULL,
  `Ucret` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `islemler`
--

LOCK TABLES `islemler` WRITE;
/*!40000 ALTER TABLE `islemler` DISABLE KEYS */;
INSERT INTO `islemler` VALUES (1,65478992344,'Halil','Özel','34fth04','Audi','Q3','2018-05-07','2018-05-08',160),(2,45678909876,'Murat','Orak','34nt5467','BMW','i5','2018-05-07','2018-05-08',200),(3,76567897654,'Onur','Mavi','34th7867','Seat','Leon','2018-05-08','2018-05-11',150),(4,56756756756,'Yavuz','Sümbül','34hn9087','Tofaş','Sahin','2018-05-08','2018-05-09',90),(5,23423423423,'Selo','Sarı','34PT4565','BMW','M3','2018-05-08','2018-05-12',300),(6,12345678985,'dasdsa','dsadsa','34nt5467','BMW','i5','2018-06-08','2018-05-09',200);
/*!40000 ALTER TABLE `islemler` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  2:05:11
