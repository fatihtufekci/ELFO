-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rentacar
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `araba`
--

DROP TABLE IF EXISTS `araba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `araba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Plaka` char(8) NOT NULL,
  `Marka` varchar(45) NOT NULL,
  `Model` varchar(45) NOT NULL,
  `Beygir` int(11) NOT NULL,
  `MotorHacmi` int(11) NOT NULL,
  `KM` int(11) NOT NULL,
  `Yil` int(11) NOT NULL,
  `AlisTarihi` date DEFAULT NULL,
  `TeslimTarihi` date DEFAULT NULL,
  `GunlukUcreti` int(11) NOT NULL,
  `Durum` tinyint(1) NOT NULL,
  `MusteriTC` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MusteriTC` (`MusteriTC`),
  CONSTRAINT `araba_ibfk_1` FOREIGN KEY (`MusteriTC`) REFERENCES `musteri` (`TC`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `araba`
--

LOCK TABLES `araba` WRITE;
/*!40000 ALTER TABLE `araba` DISABLE KEYS */;
INSERT INTO `araba` VALUES (6,'34yz8111','Renault','Symbol',100,1400,29000,2015,NULL,NULL,100,0,NULL),(7,'34tb7869','Ford','Taounus',180,1900,200000,1985,NULL,NULL,90,0,NULL);
/*!40000 ALTER TABLE `araba` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  2:05:11
