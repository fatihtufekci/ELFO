-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rentacar
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `musteriaraba`
--

DROP TABLE IF EXISTS `musteriaraba`;
/*!50001 DROP VIEW IF EXISTS `musteriaraba`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `musteriaraba` AS SELECT 
 1 AS `TC`,
 1 AS `Ad`,
 1 AS `Soyad`,
 1 AS `Telefon`,
 1 AS `Plaka`,
 1 AS `Marka`,
 1 AS `Model`,
 1 AS `AlisTarihi`,
 1 AS `TeslimTarihi`,
 1 AS `GunlukUcreti`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `musteriaraba`
--

/*!50001 DROP VIEW IF EXISTS `musteriaraba`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `musteriaraba` AS (select `m`.`TC` AS `TC`,`m`.`Ad` AS `Ad`,`m`.`Soyad` AS `Soyad`,`m`.`Telefon` AS `Telefon`,`a`.`Plaka` AS `Plaka`,`a`.`Marka` AS `Marka`,`a`.`Model` AS `Model`,`a`.`AlisTarihi` AS `AlisTarihi`,`a`.`TeslimTarihi` AS `TeslimTarihi`,`a`.`GunlukUcreti` AS `GunlukUcreti` from (`musteri` `m` join `araba` `a` on((`m`.`TC` = `a`.`MusteriTC`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'rentacar'
--

--
-- Dumping routines for database 'rentacar'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  2:05:11
